package com.example.dheerajkumarreddy.getlocation;

import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class MainActivity extends AppCompatActivity {

    private FusedLocationProviderClient client;
    public String url = "192.168.44.2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requestPermission();

        client = LocationServices.getFusedLocationProviderClient(this);

        Button button = findViewById(R.id.getLocation);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ActivityCompat.checkSelfPermission(MainActivity.this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }

                client.getLastLocation().addOnSuccessListener(MainActivity.this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        Socket s;
                        PrintWriter writer;


                        if(location!=null)
                        {
                            TextView textView = findViewById(R.id.location);
                            textView.setText(location.toString());
                            String message1 = location.toString();
                            public void display {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                            alertDialogBuilder.setMessage("Your Location is being sent to IP Address : " + url);
                            AlertDialog alertDialog1 = alertDialogBuilder.create();
                            alertDialog1.show();
                        }
                            BackgroundTask b1 = new BackgroundTask();
                            b1.execute(message1);

                        }

                    }
                });
            }
        });
    }

    class BackgroundTask extends AsyncTask<String,Void,Void>
    {
        Socket s;
        PrintWriter writer;

        @Override
        protected Void doInBackground(String... voids) {
            try
            {
                String message = voids[0];
                s = new Socket("192.168.144.2",7000);
                writer = new PrintWriter(s.getOutputStream());
                writer.write(message);
                writer.flush();
                writer.close();


            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }


    private void requestPermission(){

        ActivityCompat.requestPermissions(this,new String[]{ACCESS_FINE_LOCATION}, 1);
        ActivityCompat.requestPermissions(this,new String[]{ACCESS_COARSE_LOCATION},1);
    }
}
