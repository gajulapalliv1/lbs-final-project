public aspect AOP {


pointcut doInBackground(String voids):execution(* BackgroundTask.doInBackground(String)) && args(voids) ;

 @Around("execution(* *.*(..))")
    public void doInBackground(String voids) { ... }


}